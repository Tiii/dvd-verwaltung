package de.htwsaar.dvdtool.exception;

/**
*   Regisseur Fehlermeldung
*
*   @author Timo Klein
*/
public class DirectorException 
     extends RuntimeException
{
    private final static String EXN_MESSAGE = "Es ist ein Fehler in der Director-Klasse aufgetreten!";
    
    public DirectorException() {
        super(EXN_MESSAGE);
    }

    public DirectorException(String exnMsg) {
        super(exnMsg);
    }
}
