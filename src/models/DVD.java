package de.htwsaar.dvdtool.models;

import de.htwsaar.dvdtool.exception.DVDException;
import de.htwsaar.dvdtool.models.Director;
import de.htwsaar.dvdtool.models.Studio;

/**
*   DVD Object
*
*   @author Timo Klein
*
*/
public class DVD {
    
    private int id;
    private int timeInMinutes;
    private String title;
    private Director director;
    private Studio studio;

    /**
    *   Constructor
    *
    *   @param id               Die Identifikationsnummer der DVD
    *   @param title            Der Titel der DVD
    *   @param timeInMinutes    Die Spieldauer der DVD
    *
    */
    public DVD(int id, String title, int timeInMinutes) {
        setId(id);
        setTitle(title);
        setTime(timeInMinutes);
    }
    
    /**
    *   Zuweisen der DVD-ID
    *
    *   @param id  ID der DVD in der Datenbank 
    */
    public void setId(int id) {
        checkParam(id > 0,ID_INCORRECT);
        this.id = id;
    }

    /**
    *   Zugriff auf die ID
    *
    *   @return int Die ID des DVD-Objektes 
    */
    public int getId() {
        return this.id;
    }

    /**
    *   Setzten der Spieldauer der DVD
    *
    *   @param time Zeit in Minuten
    */
    public void setTime(int time) {
        checkParam(time > 0 , TIME_NOT_SET);
        this.timeInMinutes = time;
    }

    /**
    *   Spieldauer der DVD abfragen
    *
    *   @return int Zeit in Minuten
    */
    public int getTime() {
        return this.timeInMinutes;
    }
    
    /**
    *   Setzen des DVD-Titel
    *   
    *   @param title    DVD-Titel
    */
    public void setTitle(String title) {
        checkParam(title != null, TITLE_NOT_NULL);
        this.title = title;
    }

    /**
    *   Titel der DVD abfragen  
    *
    *   @return String Titel der DVD
    */
    public String getTitle() {
        return this.title;
    }

    /**
    *   Hilfsmethode zur Ueberpruefung von Variablen
    */
    private void checkParam(boolean condition, String exnMsg) {
        if(!condition)
            throw new DVDException(exnMsg);
    }
    
    // Exception-Meldungen
    private final static String ID_INCORRECT    = "ID wurde nicht gefunden";
    private final static String TITLE_NOT_NULL  = "Title darf nicht leer sein!";
    private final static String TIME_NOT_SET    = "Spieldauer darf nicht leer sein!";
}
