package de.htwsaar.dvdtool.exception;


/**
*   Studio Exception
*
*   @author Timo Klein
*/
public class StudioException
     extends RuntimeException
{

    private final static String EXN_MESSAGE = "Es ist ein Fehler im Studio Object aufgetreten";

    public StudioException() {
        super(EXN_MESSAGE);
    }

    public StudioException(String exnMsg) {
        super(exnMsg);
    }
}
