package de.htwsaar.dvdtool.models;

import de.htwsaar.dvdtool.exception.DirectorException;
/**
*   Filmregisseur
*
*   @author Timo Klein
*
*/
public class Director {
    
    private int id;
    private String name;
    
    /**
    *   Kostruktor
    *   
    *   @param id   ID des Directors in der Datenbank
    *   @param name Name des Regisseur
    */
    public Director(int id, String name) {
        setId(id);
        setName(name);
    }
    
    /**
    *   Setzten der ID
    *   
    *   @param id Setzten der ID aus der Datenbank
    */
    public void setId(int id) {
        checkParam(id > 0, ID_INCORRECT);
        this.id = id;
    }
    
    /**
    *   Zugriff auf die ID
    *
    *   @return int ID des Regisseur
    */
    public int getId() {
        return this.id;
    }

    /**
    *   Name des Regisseur setzten
    *
    *   @param name Name des Regisseur
    */
    public void setName(String name) {
        checkParam(name != null, NAME_INCORRECT);
        this.name = name;
    }

    /**
    *   Name des Regisseur erhalten
    *
    *   @return name Name des Regisseur
    */
    public String getName() {
        return this.name;
    }
    
    /**
    *   Hilfsmethode um Variablen zu pruefen
    */
    private void checkParam(boolean condition, String exnMsg) {
        if (!condition)
            throw new DirectorException(exnMsg);
    }
    
    // Exception-Meldungen
    private final static String ID_INCORRECT    = "ID konnte nicht zugewiesen werden!";
    private final static String NAME_INCORRECT  = "Name darf nicht leer sein!";
}
