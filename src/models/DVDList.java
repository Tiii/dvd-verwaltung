package de.htwsaar.dvdtool.models;

import de.htwsaar.dvdtool.models.DVD;
import java.util.*;

/**
*   Liste mit allen DVD´s
*
*   @author Timo Klein
*
*/
public class DVDList {
    
    private List<DVD> list;

    public DVDList() {
        list = new ArrayList<DVD>();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }
    
    public boolean isFull() {
        return false;
    }

    public void add(DVD dvd) {
        if(list == null)
            throw new RuntimeException(); 

        if(dvd == null)
            throw new RuntimeException();
        
        if(!list.contains(dvd))
            list.add(dvd);
    }

    public int getDVDCount() {
        return list.size();
    }

}
