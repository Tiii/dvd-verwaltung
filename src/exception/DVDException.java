package de.htwsaar.dvdtool.exception;


/**
*   DVD Exception
*
*   @author Timo Klein
*/
public class DVDException
     extends RuntimeException
{

    private final static String EXN_MESSAGE = "Es ist ein Fehler im DVD Object aufgetreten";

    public DVDException() {
        super(EXN_MESSAGE);
    }

    public DVDException(String exnMsg) {
        super(exnMsg);
    }
}
