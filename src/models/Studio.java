package de.htwsaar.dvdtool.models;

import de.htwsaar.dvdtool.exception.StudioException;

/**
*   Filmstudio
*
*   @author Timo Klein
*/
public class Studio {

    private int id;
    private String name;
    
    /**
    *   Konstruktor
    *
    *   @param id   ID des Studios in der Datenbank
    *   @param name Name des Studio
    */
    public Studio(int id, String name) {
        setId(id);
        setName(name);
    }
    
    /**
    *   Setzten der ID
    *
    *   @param id ID des Studio
    */
    public void setId(int id) {
        checkParam(id > 0, ID_INCORRECT);
        this.id = id;
    }
    
    /**
    *   Abfrage der Studio-ID
    *
    *   @return int Studio-ID
    */
    public int getId() {
        return this.id;
    }

    /**
    *   Name des Studio setzen
    *
    *   @param name Name des Studio (z.B Pixar)
    */
    public void setName(String name) {
        checkParam(name != null, NAME_NOT_NULL);
        this.name = name;
    }
    
    /**
    *   Abfrage des Studio-Namen
    *   
    *   @return String Name des Studios
    */
    public String getName() {
        return this.name;
    }

    /**
    *   Hilfsmethode zur Variblenpruefung
    */
    private void checkParam(boolean condition, String exnMsg) {
        if(!condition)
            throw new StudioException(exnMsg);
    }
    
    // Exception_Meldungen
    private final static String ID_INCORRECT    = "ID wurde nicht gefunden!";
    private final static String NAME_NOT_NULL   = "Name darf nicht leer sein!";
}
